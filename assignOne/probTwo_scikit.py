import numpy as np
import scipy.io
from matplotlib import pyplot as plt
from sklearn import linear_model

# load data from file
data = scipy.io.loadmat('data2.mat')['data']

# number of examples
n = data.shape[0]

# features and targets
X = data[:, :2]
y = data[:, 2]

# regularize features
X = (X - np.mean(X, axis=0))/np.std(X, axis=0)

# create instance of logistic regression class
lgrg = linear_model.LogisticRegression()

# perform logistic regression
lgrg.fit(X, y)

# extract coefficients
coef = lgrg.coef_

# evaluate performance of model
pred = lgrg.predict(X)
correct = np.mean(y == pred)

# split positive and negative examples
pos_ind = data[:, 2] == 1
neg_ind = data[:, 2] == 0

# create decisino boundary
f1_d = np.linspace(X[:, 0].min(), X[:, 0].max(), 1000)
fcn = (-coef[:, 0] - coef[:, 0]*f1_d)/coef[:, 1] 

# plot decision boundary
plt.figure(1)
plt.figure(figsize=(8, 8))
plt.scatter(X[pos_ind, 0], X[pos_ind, 1], c = 'g')
plt.scatter(X[neg_ind, 0], X[neg_ind, 1], c = 'r')
plt.plot(f1_d, fcn)
plt.legend(['decision boundary', 'class one', 'class two'])
plt.title('Batch Gradient Descent', fontsize=16)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.savefig('p2_batch.png')

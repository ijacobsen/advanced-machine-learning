# linear regression

import numpy as np
import scipy.io
from matplotlib import pyplot as plt
from sklearn import linear_model
from sklearn.model_selection import train_test_split

# load data from file
data = scipy.io.loadmat('data1.mat')['data']

# randomly split data into training and cross validation
x_train, x_cv, y_train, y_cv = train_test_split(data[:, 0], data[:, 1],
                                                    test_size = 0.4)
# number of training and cv examples
n = x_train.shape[0]
N = x_cv.shape[0]

# vary the order of the polynomial to regress over
d_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
model = {}


# training
train_error = np.zeros((len(d_list), 1))
for d in d_list:
    
    # create vector of powers of x
    powers = np.arange(d+1)
    
    # calculate [x^0, x^1, ..., x^d]
    x_feat_train = np.power(x_train.reshape(n, 1), powers)

    # create linear regression object
    regr = linear_model.LinearRegression()

    # fit linear model    
    regr.fit(x_feat_train, y_train)
    
    # evaluate performance of model on training data
    train_error[d-1] = np.sqrt(np.mean((regr.predict(x_feat_train) -
                               y_train) ** 2))
    
    # store entire model in dictionary for cross validation purposes
    model[d] = regr
              
# cross validation
cv_error = np.zeros((len(d_list), 1))
for d in d_list:
    
    # create vector of powers of x
    powers = np.arange(d+1)
    
    # calculate [x^0, x^1, ..., x^d]
    x_feat_cv = np.power(x_cv.reshape(N, 1), powers)
    
    # evaluate performance of model on cross validation data
    cv_error[d-1] = np.sqrt(np.mean((model[d].predict(x_feat_cv) -
                            y_cv) ** 2))
    
# plot performance
plt.figure(1)
plt.clf()
plt.figure(figsize=(8, 8))
plt.plot(train_error)
plt.plot(cv_error)
plt.title('Training and Cross Validation Error', fontsize = 16)
plt.legend(['training error', 'cross validation error'])
plt.xlabel('order of polynomial')
plt.ylabel('RMS error')
plt.savefig('p1_RMS_error.png')

# plot estimated functions
plt.figure(2)
plt.clf()
plt.figure(figsize=(16, 8))
legend_list = []
for d in d_list:
    
    # create vector of powers of x
    powers = np.arange(d+1)
    
    # domain
    x_domain = np.linspace(np.min(data[:, 0]), np.max(data[:, 0]),
                                  10000).reshape(10000, 1)
    
    # calculate [x^0, x^1, ..., x^d]
    fcn_domain = np.power(x_domain, powers)
    
    # function estimate
    fcn = model[d].predict(fcn_domain)
    
    # plot function
    plt.plot(x_domain, fcn) 
    legend_list.append('d = {}'.format(d))

plt.legend(legend_list)
plt.title('Estimated Functions', fontsize = 16)
plt.xlabel('independent variable')
plt.ylabel('dependent variable')
plt.savefig('p1_functions.png')

# plot actual data, and best model
best_index = np.argmin(cv_error)
x_domain = np.linspace(np.min(x_cv), np.max(x_cv), 10000).reshape(10000, 1)
d = d_list[best_index]
fcn_domain = np.power(x_domain, np.arange(d+1))
fcn = model[d].predict(fcn_domain)
plt.figure(3)
plt.figure(figsize=(8, 8))
plt.scatter(x_cv, y_cv, s=10)
plt.plot(x_domain, fcn, 'red')
plt.legend(['function estimate', 'cross validation data'])
plt.title('Best Estimated Function and Cross Validation Data', fontsize=16)
plt.xlabel('independent variable')
plt.ylabel('dependent variable')
plt.savefig('p1_superimposed.png')




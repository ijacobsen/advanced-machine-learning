import numpy as np
import scipy.io
from matplotlib import pyplot as plt
from scipy.special import xlogy

# sigmoid function
h = lambda x: 1/(1+np.exp(-x))

# empirical risk
L = lambda theta, y, x: np.mean((y - 1) * xlogy(np.sign(1-h(np.dot(x, 
                                theta.T))), 1-h(np.dot(x, theta.T))) -
                                y*xlogy(np.sign(h(np.dot(x, theta.T))), 
                                        h(np.dot(x, theta.T))))

# load data from fileemp_
data = scipy.io.loadmat('data2.mat')['data']

# number of examples
n = data.shape[0]

# features and targets
X = data[:, :2]
y = data[:, 2].reshape(n, 1)

# regularize features
X = (X - np.mean(X, axis=0))/np.std(X, axis=0)

# add ones
X = np.hstack((np.ones((n, 1)), X))

# initalize theta
theta = np.zeros((1, 3))

# convergence threshold and learning rate
eta = 1
num_steps = 15
error = np.zeros((num_steps, 1))
emp_risk = np.zeros((num_steps, 1))

# perform logistic regression
for j in range(num_steps):

    # calculate gradient
    d = np.dot(X.T, (y - h(np.dot(X, theta.T)))).T

    # update theta
    theta = theta + eta * d

    # calculate error
    error[j] = np.sum(np.round(h(np.dot(X, theta.T))) != y)/float(n)

    # calculate emperical risk
    emp_risk[j] = L(theta, y, X)

# plot error and emperical risk vs iterations
plt.figure(1)
plt.figure(figsize=(16, 8))
plt.suptitle('Performance of BGD with Time', fontsize=16)
plt.subplot(2, 1, 1)
plt.plot(error)
plt.xlabel('Iterations')
plt.ylabel('Classification Error')
plt.ylim([0, 1])
plt.subplot(2, 1, 2)
plt.plot(emp_risk)
plt.xlabel('Iterations')
plt.ylabel('Emperical Risk')
plt.savefig('p2_BGD_error.png')

# split positive and negative examples
pos_ind = data[:, 2] == 1
neg_ind = data[:, 2] == 0

# create decisino boundary
f1_d = np.linspace(X[:, 1].min(), X[:, 1].max(), 1000)
fcn = (theta[:, 0] + theta[:, 1]*f1_d)/(-theta[:, 2])

# plot decision boundary
plt.figure(2)
plt.figure(figsize=(8, 8))
plt.scatter(X[pos_ind, 1], X[pos_ind, 2], c='g')
plt.scatter(X[neg_ind, 1], X[neg_ind, 2], c='r')
plt.plot(f1_d, fcn)
plt.legend(['decision boundary', 'class one', 'class two'])
plt.title('Binary Classification using BGD', fontsize=16)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.savefig('p2_BGD.png')

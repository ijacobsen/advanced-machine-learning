from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

import tensorflow as tf
sess = tf.InteractiveSession()

import numpy as np

x = tf.placeholder(tf.float32, shape=[None, 784, 2])
y_ = tf.placeholder(tf.float32, shape=[None, 2])

def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')

def balance_training(images, labels, train_size):
    
    
    # generate a large random index
    ind = np.random.randint(0, train_size, 10000).reshape(5000, 2)
    ind_0, ind_1 = ind[:, 0], ind[:, 1]

    # find matching pairs
    y_labels = np.sum(np.logical_and(labels[ind_0, :], labels[ind_1, :]),
                      axis=1).reshape(5000, 1)

    match_ind = np.where(y_labels == 1)[0]

    # batch size of 400, we want 200 matching, and 200 not matching
    use_ind_0 = np.hstack((ind_0[match_ind[:50]],
                           np.random.randint(0, train_size,
                                             50))).reshape(100, 1)
    use_ind_1 = np.hstack((ind_1[match_ind[:50]],
                           np.random.randint(0, train_size,
                                             50))).reshape(100, 1)
    use_ind = np.hstack((use_ind_0, use_ind_1))

    # shuffle deck, preserving index
    N = np.arange(100)
    np.random.shuffle(N)
    ind = use_ind[N, :]
    
    # get images
    x_stacked = np.dstack((images[ind[:, 0], :],
                           images[ind[:, 1]]))
    
    # get labels
    y_stacked = np.sum(np.logical_and(labels[ind[:, 0], :],
                                      labels[ind[:, 1], :]),
                       axis=1).reshape(100, 1)
    y_stacked = np.hstack((y_stacked, np.sum(np.logical_not(y_stacked),
                                             axis=1).reshape(100, 1)))
    
    # return approximately equally balanced training set
    return x_stacked, y_stacked

W_conv1 = weight_variable([3, 3, 2, 128])
b_conv1 = bias_variable([128])

x_image = tf.reshape(x, [-1,28,28,2])

h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
h_pool1 = max_pool_2x2(h_conv1)

W_conv2 = weight_variable([3, 3, 128, 256])
b_conv2 = bias_variable([256])

h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

W_fc1 = weight_variable([7 * 7 * 256, 2048])
b_fc1 = bias_variable([2048])

h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*256])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

W_fc2 = weight_variable([2048, 2])
b_fc2 = bias_variable([2])

y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2
                  

cross_entropy = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))
train_step = tf.train.AdamOptimizer(1e-3).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
sess.run(tf.global_variables_initializer())

train_images = mnist.train.images
train_labels = mnist.train.labels
train_size = mnist.train.num_examples

for i in range(5000):
  x_stacked, y_stacked = balance_training(train_images, train_labels,
                                            train_size)
  if i%10 == 0:
    train_accuracy = accuracy.eval(feed_dict={
        x:x_stacked, y_: y_stacked, keep_prob: 1.0})
    print("step %d, training accuracy %g"%(i, train_accuracy))
  train_step.run(feed_dict={x: x_stacked, y_: y_stacked, keep_prob: 0.5})


# test accuracy of model
x_stacked_test = np.dstack((mnist.test.images[:5000, :],
                            mnist.test.images[5000:, :]))
# get labels
y_stacked_test = np.sum(np.logical_and(mnist.test.labels[:5000, :],
                                       mnist.test.labels[5000:, :]),
                        axis=1).reshape(5000, 1)
y_stacked_test = np.hstack((y_stacked_test, np.sum(np.logical_not(y_stacked_test),
                                                   axis=1).reshape(5000, 1)))

print('test accuracy %g'%accuracy.eval(feed_dict={x: x_stacked_test,
                                                  y_: y_stacked_test,
                                                  keep_prob: 1.0}))

import numpy as np
from matplotlib import pyplot as plt

file_name = 'out_gpu'
data = np.loadtxt(open(file_name, 'rb'), delimiter=' ')

# plot
plt.figure(figsize=(5, 5))
plt.title('Classification Accuracy v.s. Training Time', fontsize=18)
plt.plot(data[:, 0])
plt.plot(data[:, 1])
plt.ylabel('Accuracy')
plt.xlabel('Epochs')
plt.ylim([0, 1])
plt.legend(['Training Accuracy', 'Testing Accuracy'], loc='best')
plt.savefig('p4_acc.png')

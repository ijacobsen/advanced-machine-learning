'''
generate a 2-D dataset, then use the function f(x, y) = x^2 + xy + y^2

design a fully connected network with at least one hidden layer to predict
the value of f given (x, y).

use 10% as testing, and the rest as training data.

use MSE (mean-squared-error) as the loss function.

show the architecture of the design, plot the train and test loss as a fcn
of training time (report loss after each mini-batch), and report the final
training and testing loss.
'''

import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt

#f = lambda x: (x**2).reshape(len(x), 1)
f = lambda x, y: (x**2 + x*y + y**2).reshape(len(x), 1)

# ----------------------- generate data ---------------------------
data_num = 5000
data_x = np.random.uniform(-10, 10, (data_num, 1))
data_y = np.random.uniform(-10, 10, (data_num, 1))
data = np.hstack((data_x, data_y))

# training data
training_size = int(0.9*data_num)
train_data = data[:training_size, :]
train_targ = f(train_data[:, 0], train_data[:, 1])


# testing data
testing_size = int(0.1*data_num)
test_data = data[-testing_size:, :]
test_targ = f(test_data[:, 0], test_data[:, 1])

# plot data
# plt.scatter(train_data, train_targ)

# set some parameters
learning_rate = .01
training_epochs = 200
batch_size = 20
display_step = 10

# network parameters
n_input = 2
n_hidden_1 = 12
n_out = 1

# create interactive session object
sess = tf.InteractiveSession()

# create nodes for the input and target output classes
x = tf.placeholder(tf.float32, shape=[None, n_input])     # 2 input features
y = tf.placeholder(tf.float32, shape=[None, n_out])    # 4 output classes


# create model
def multilayer_perceptron(x, weights, biases):
    layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
    layer_1 = tf.nn.relu(layer_1)
    out_layer = tf.matmul(layer_1, weights['out']) + biases['out']
    return out_layer

weights = {
        'h1': tf.Variable(tf.random_normal([n_input, n_hidden_1])),
        'out': tf.Variable(tf.random_normal([n_hidden_1, n_out]))
}

biases = {
        'b1': tf.Variable(tf.random_normal([n_hidden_1])),
        'out': tf.Variable(tf.random_normal([n_out]))
}

# construct model
pred = multilayer_perceptron(x, weights, biases)

# define loss and optimizer
# cost = tf.reduce_mean(tf.nn.l2_loss(pred - y))
# cost = tf.reduce_mean(tf.square(pred - y))
cost = tf.reduce_sum(tf.pow(pred-y, 2))/(2*training_size)
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# initialize the variables
init = tf.global_variables_initializer()

# launch the graph
#with tf.Session() as sess:
sess.run(init)

train_loss = np.zeros([training_epochs, 1])
test_loss = np.zeros([training_epochs, 1])

# training
for epoch in range(training_epochs):
    total_batch = int(training_size/batch_size)
    avg_cost = 0.
    # loop over all batches
    for i in range(total_batch):
        b_ind = np.random.uniform(0, training_size,
                                  size=batch_size).astype(int)
        batch_x, batch_y = train_data[b_ind, :], train_targ[b_ind, :]

        # run backprop and cost op to get loss value
        _, c = sess.run([optimizer, cost], feed_dict={x: batch_x,
                                                      y: batch_y})
        avg_cost += c/total_batch

    c_test = sess.run(cost, feed_dict={x: test_data, 
                                       y: test_targ})
    train_loss[epoch] = avg_cost
    test_loss[epoch] = c_test
    # display logs per epoch step
    if epoch % display_step == 0:
        print('Epoch: {}, Cost: {}'.format(epoch, avg_cost))
print('Optimization finished')

# evaluate performance
pred_train_targ = sess.run(pred, feed_dict={x: train_data})
pred_test_targ = sess.run(pred, feed_dict={x: test_data})

train_cost = sess.run(cost, feed_dict={x: train_data, y: train_targ})
test_cost = sess.run(cost, feed_dict={x: test_data, y: test_targ})


ref = np.arange(np.ceil(test_targ.max()))
plt.figure(figsize=(5, 5))
plt.title('Correlation of True v.s Predicted Targets', fontsize=18)
plt.scatter(test_targ, pred_test_targ, s=10)
plt.plot(ref, 'r')
plt.ylabel('Target')
plt.xlabel('Target')
plt.legend(['Corr=1 (Reference)', 'Correlation'])
plt.savefig('p2_corr.png')

plt.figure(figsize=(5, 5))
plt.title('MSE Loss v.s. Training Time', fontsize=18)
plt.plot(train_loss)
plt.plot(test_loss)
plt.ylabel('Cost')
plt.xlabel('Epochs')
plt.ylim([0, 5])
plt.legend(['Training Cost', 'Testing Cost'])
plt.savefig('p2_cost.png')

print('Train cost: {}'.format(train_cost))
print('Test cost: {}'.format(test_cost))

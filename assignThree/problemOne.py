'''
design a fully connected network with at least one hidden layer that
solves the classification problem.

use 10% as testing, and the remaining as training data.

use NLL (negative log-likelihood) as the loss function.

show the architecture of the design.

plot the training and test loss as a function of training time... report loss
after each data mini-batch.

report final training and test accuracy.
'''

import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt

# load data from file
raw_data = np.loadtxt(open('Data.csv', 'rb'), delimiter=',')

# features
data = raw_data[:, :2]

# targets
s_targs = raw_data[:, 2].reshape(data.shape[0], 1).astype(int)

# convert scalar targets to one-hot vectors
targets = np.zeros((data.shape[0], 4))
one_hot_d = {1: np.array([1, 0, 0, 0]), 2: np.array([0, 1, 0, 0]),
             3: np.array([0, 0, 1, 0]), 4: np.array([0, 0, 0, 1])}
for i in range(s_targs.shape[0]):
    targets[i, :] = one_hot_d[s_targs[i][0]]

# stack, then shuffle
data = np.hstack((data, targets))
N = np.arange(data.shape[0])
np.random.shuffle(N)
data = data[N, :]

# ----------------------- training data ---------------------------
train_data = data[:3600, :]


# ----------------------- testing data ---------------------------
test_data = data[3600:, :]

# set some parameters
learning_rate = 0.01
training_epochs = 35
batch_size = 20
display_step = 1

# network parameters
n_input = 2
n_hidden = 3
n_classes = 4
logs_path = '~/Projects'

# create interactive session object
sess = tf.InteractiveSession()

# create nodes for the input and target output classes
x = tf.placeholder(tf.float32, shape=[None, n_input], name='InputData')     # 2 input features
y = tf.placeholder(tf.float32, shape=[None, n_classes], name='InputLabels')    # 4 output classes

# create model
def multilayer_perceptron(x, weights, biases):
    layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
    out_layer = tf.matmul(layer_1, weights['out']) + biases['out']
    return out_layer

weights = {
        'h1': tf.Variable(tf.random_normal([n_input, n_hidden]), name='WeightsH1'),
        'out': tf.Variable(tf.random_normal([n_hidden, n_classes]), name='WeightsOut')
}

biases = {
        'b1': tf.Variable(tf.random_normal([n_hidden]), name='BiasB1'),
        'out': tf.Variable(tf.random_normal([n_classes]), name='BiasOut')
}

# construct model
pred = multilayer_perceptron(x, weights, biases)

# define loss and optimizer
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred,
                                                              labels=y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# initialize the variables
init = tf.global_variables_initializer()

train_loss = np.zeros([training_epochs, 1])
test_loss = np.zeros([training_epochs, 1])

# launch the graph
with tf.Session() as sess:
    sess.run(init)
    summary_writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())
    
    total_batch = int(train_data.shape[0]/batch_size)
    # training
    for epoch in range(training_epochs):
        avg_cost = 0.
        
        # loop over all batches
        for i in range(total_batch):
            b_ind = np.random.uniform(0, train_data.shape[0],
                                      size=batch_size).astype(int)
            batch_x, batch_y = train_data[b_ind, :2], train_data[b_ind, 2:]
            
            # run backprop and cost op to get loss value
            _, c = sess.run([optimizer, cost], feed_dict={x: batch_x,
                                                          y: batch_y})

            # compute average loss
            avg_cost += c / total_batch
            
        c_test = sess.run(cost, feed_dict={x: test_data[:, :2], 
                                             y: test_data[:, 2:]})
    
        train_loss[epoch] = avg_cost
        test_loss[epoch] = c_test
        # display logs per epoch step
        if epoch % display_step == 0:
            print('Epoch: {}, Training Cost: {}, Testing Cost: {}'.format(epoch, avg_cost, c_test))
    print('Optimization finished')
    
    # test model
    correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
    
    # calculate accuracy
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, 'float'))
    print('Training Accuracy: {}'.format(accuracy.eval({x: train_data[:, :2],
                                                        y: train_data[:, 2:]})))
    print('Testing Accuracy: {}'.format(accuracy.eval({x: test_data[:, :2], 
                                                       y: test_data[:, 2:]})))
    


'''
# plot
plt.figure(figsize=(5, 5))
plt.title('NLL Cost v.s. Training Time', fontsize=18)
plt.plot(train_loss)
plt.plot(test_loss)
plt.ylabel('Cost')
plt.xlabel('Epochs')
plt.ylim([0, 1])
plt.legend(['Training Cost', 'Testing Cost'])
plt.savefig('p1_cost.png')
'''

'''
implement the linear perceptron using SGD and gradient descent.

use data3.mat, and use the whole set as training.
    - each row consists of the feature vector followed by the label.

show the resulting linear decision boundary.

show the evolution of binary classification error and the perceptron error
with time from random initialization until convergence on a successful run *
    *** note: not all randomly initalized runs will converge!

discuss the convergence behavior as the step size is varied.
'''
import numpy as np
import scipy.io
from matplotlib import pyplot as plt

# load data from file
file_name = 'data3.mat'
data = scipy.io.loadmat(file_name)['data']

# number of training examples
N = data.shape[0]

# separate features from targets
x = np.hstack((np.ones((N, 1)), data[:, :2]))
y = data[:, -1].reshape(N, 1)

# initalize weight vector close to 0
w = np.random.uniform(0, 0.01, (3, 1))
error_o = 1
err = []

converged = False
epsilon = 1e-7
eta = .5
j=0
#while not converged:
for i in range(20000):
    
    # pick random training example
    i = np.random.randint(0, N)

    # check if i is misclassified
    if (y[i] * np.dot(x[i], w) <= 0):
        w_n = w + eta * (y[i] - np.dot(x[i], w)) * x[i].reshape(3, 1)

    # find classification error
    y_hat = np.where(np.dot(x, w_n) > 0, 1, -1)
    error = np.mean(np.abs(y - y_hat))

    # check for convergence
    if ((error - error_o)/error_o < epsilon):
        converged = True

    # update weight vector
    w = w_n
    
    error_o = error
    
    err.append(error_o)


pos = y == 1
neg = y == -1
x1 = np.linspace(0, 1, 1000)
x2 = (w[0] + w[1] * x1) / (-w[2])

# plot decision boundary
plt.figure(figsize=(6, 6))
plt.title('Decision Boundary of Single Layer Perceptron', fontsize=18)
plt.scatter(x[pos[:, 0], 1], x[pos[:, 0], 2])
plt.scatter(x[neg[:, 0], 1], x[neg[:, 0], 2])
plt.plot(x1, x2)
plt.xlabel('x1', fontsize=16)
plt.ylabel('x2', fontsize=16)
plt.legend(['Decision Boundary', 'Class 1', 'Class 2'])
plt.savefig('p1_decision_boundary.png', dpi = 100)

# plot binary classification error with time
plt.figure(figsize=(6, 6))
plt.title('Binary Classification Error', fontsize=18)
plt.plot(err)
plt.xlabel('Iterations', fontsize=16)
plt.ylabel('Classification Error', fontsize=16)
plt.savefig('p1_classification_error.png', dpi = 100)


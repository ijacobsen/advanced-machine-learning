import numpy as np
from sklearn import svm
import scipy.io
from matplotlib import pyplot as plt

filename = 'shoesducks.mat'
raw_data = scipy.io.loadmat(filename)

data = raw_data['X']
labels = raw_data['Y']

train_data = data[:72, :]
train_labels = labels[:72, :]

test_data = data[72:, :]
test_labels = labels[72:]

'''
c_list = [0.1, 1, 3, 10]
gamma_list = [0.01, 0.1, 1, 3]

for c in c_list:
    for gamma in gamma_list:

        clf = svm.SVC(gamma=gamma, C=c, kernel='rbf')

        clf.fit(train_data, train_labels)

        pred = clf.predict(test_data).reshape(72, 1)
        accuracy = np.mean(pred == test_labels)

        print(gamma, c, accuracy)
'''


c_list = [0.01, 0.1, 3, 100]

for c in c_list:

    clf = svm.SVC(C=c, kernel='linear')

    clf.fit(train_data, train_labels)

    pred = clf.predict(test_data).reshape(72, 1)
    accuracy = np.mean(pred == test_labels)

    print(c, accuracy)

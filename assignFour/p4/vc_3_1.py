import numpy as np
from matplotlib import pyplot as plt

x = np.linspace(-1, 1, 100)
y = np.linspace(-1, 1, 100)
X, Y = np.meshgrid(x, y)

plus = np.array([[-1, 1], [-1, 1]])
minus = np.array([[0], [0]])

z = X**2 + Y**2

plt.figure(figsize=(3, 3))
plt.contour(X, Y, z, [1])
plt.scatter(plus[0], plus[1], marker='v', c='r')
plt.scatter(minus[0], minus[1], marker='^')
plt.savefig('bad_class.png', dpi=100)


pos = [[[-1, -np.sqrt(3)], [0, 0], [1, -np.sqrt(3)]], [[-1, -np.sqrt(3)], [0, 0]], [[-1, -np.sqrt(3)], [1, -np.sqrt(3)]],
       [[0, 0], [1, -np.sqrt(3)]], [[-1, -np.sqrt(3)]], [[0, 0]], [[1, -np.sqrt(3)]], [[]]]

neg = list(pos)
neg.reverse()

fig, axs = plt.subplots(4, 2, figsize=(6, 10))
axs = axs.ravel()
for i in range(0, 8):
    ax = axs[i]
    plus = np.array(pos[i])
    minus = np.array(neg[i])
    if plus.shape[1] != 0:
        dx = np.mean(plus[:, 0])
        dy = np.mean(plus[:, 1])
        circle = plt.Circle((dx, dy), 1.2, fill=False)
        ax.add_artist(circle)
        ax.scatter(plus[:, 0], plus[:, 1])
    ax.set_ylim((-2.5, 1))
    ax.set_xlim((-2, 2))
    ax.set_aspect(1)
    if minus.shape[1] != 0:
        ax.scatter(minus[:, 0], minus[:, 1], marker='^', c='r')

fig.savefig('VC_dim.png', dpi=100)
